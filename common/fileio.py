import os
import re
import time
from collections import deque
from csv import DictWriter
from json import dump

import dask.dataframe as dd
import pandas as pd


def write_csv_dict(output_file: str, dict_list: [{}], columns: [str]):
    with open(output_file, 'w', newline='') as output_csv:
        writer = DictWriter(output_csv, fieldnames=columns)
        writer.writeheader()

        for row in dict_list:
            writer.writerow(
                {key: value for key, value in row.items() if key in columns})


def write_json(output_file: str, params):
    with open(output_file, "w") as output_json:
        dump(params, output_json)


def read_csv_pandas(path, dtypes=None):
    return pd.read_csv(path, dtype=dtypes)


def write_csv_pandas(df: pd.DataFrame, path: str, columns=None):
    if columns:
        df = df.reindex(columns=columns)
    df.to_csv(path, index=False)
    return os.path.exists(path)


def read_csv_dask(path, dtypes=None, skiprows=None):
    if _is_faulty(path):
        _remove_last_line(path)

    try:
        df = dd.read_csv(path, skiprows=[skiprows], dtype=dtypes)
        df.tail(10)
    except TypeError:
        df = None

    return df


def _is_faulty(path: []):
    is_faulty = False

    with open(path, 'r') as f:
        if not re.match("^[0-9]+$", deque(f, 1)[0].split(',')[0]):
            is_faulty = True

    return is_faulty


def _remove_last_line(path: str):
    last_line_deleted = False

    with open(path, "r+", encoding="utf-8") as file:

        # find end of file
        file.seek(0, os.SEEK_END)

        # skip the last character
        pos = file.tell() - 1

        # search for the newline character (i.e. the last row)
        while pos > 0 and file.read(1) != "\n":
            pos -= 1
            file.seek(pos, os.SEEK_SET)

        # delete the last line
        if pos > 0:
            file.seek(pos, os.SEEK_SET)
            file.truncate()
            last_line_deleted = True

        # wait until operation is finished
        time.sleep(5)

    return last_line_deleted
