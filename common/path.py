import os


def create_path(path: [str]):
    return os.path.join(*path)


def check_path(path: [str], create: bool) -> str:
    path = create_path(path)

    if create:
        os.makedirs(path, exist_ok=True)

    if os.path.exists(path):
        return path
    else:
        print(path + ' not found')
