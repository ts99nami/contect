"""
CLEANING METHODS
"""

import numpy as np
from skimage.morphology import (binary_closing, binary_dilation, closing,
                                remove_small_objects, square)


def close_gaps_binary(arr: np.array) -> np.array:
    return binary_closing(arr)


def filter_small_objects(arr: np.array, max_size: int, is_bool: bool = False) -> np.array:
    if is_bool:
        arr = arr.astype(bool)
    return remove_small_objects(arr, max_size)


def dilate_binary(arr: np.array) -> np.array:
    return binary_dilation(arr)


def threshold_binary(img: np.array, threshold: int) -> np.array:
    return img > np.percentile(img, threshold)


def close_gaps_square(img: np.array, square_size: int) -> np.array:
    return closing(img, square(square_size))
