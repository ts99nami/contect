"""
LABELING
"""
import numpy as np
from skimage.draw import line as sk_line
from skimage.draw import line_aa as sk_line_aa
from skimage.measure import regionprops_table
from skimage.morphology import label


def label_segments(img: np.array) -> np.array:
    return label(img, return_num=False)


def filter_labels(img: np.array, label_list: [int]) -> np.array:
    mask = np.zeros(img.shape, dtype=int)

    for ix, iy in np.ndindex(img.shape):
        if img[ix, iy] in label_list:
            mask[ix, iy] = True
        else:
            mask[ix, iy] = False
    return mask


def _dimension_ratio(img: np.array) -> float:
    props = regionprops_table(img, properties=(
        'major_axis_length', 'minor_axis_length'))

    if props['major_axis_length'][0] > 0 and props['minor_axis_length'][0] > 0:
        ratio = props['major_axis_length'][0] / props['minor_axis_length'][0]
    else:
        ratio = 0

    return ratio


def get_line_type_segments(labeled: np.array, intersections: [int], ratio_threshold: int) -> [int]:
    line_type_segments = []

    for i_label in intersections:
        mask = labeled == i_label
        mask = mask.astype(np.uint8)  # np.array(mask, dtype=np.uint8)
        ratio = _dimension_ratio(mask)

        if ratio and ratio >= ratio_threshold:
            line_type_segments.append(i_label)

    return list(set(line_type_segments))


def _peaks_to_points(angle, dist, shape):
    y0 = _correct_value((dist - 0 * np.cos(angle) / np.sin(angle)), shape[0])
    y1 = _correct_value(
        (dist - shape[1] * np.cos(angle) / np.sin(angle)), shape[1])
    x0 = 0
    x1 = _correct_value(shape[1], shape[1])

    points = (x1, x0, y1, y0)

    if not all(points):
        points = None

    return points


def _correct_value(val, shape):
    if type(val) in [float("-inf"), float("inf"), float("nan"), float("NaN")] or not val:
        return None

    try:
        val = round(val)
    except (ValueError, OverflowError):
        return None

    if val >= shape:
        val = shape - 1
    if val <= -shape:
        val = -shape + 1
    return int(val)


# TODO: refactor
def get_line_label_intersections(labeled: np.array, line_peaks, shape: (int, int)) -> [int]:
    intersections = []

    for _, angle, dist in zip(*line_peaks):

        if angle == 0.0:
            continue

        y0 = _correct_value(
            (dist - 0 * np.cos(angle) / np.sin(angle)), shape[0])
        y1 = _correct_value(
            (dist - shape[1] * np.cos(angle) / np.sin(angle)), shape[1])
        x0 = 0
        x1 = _correct_value(shape[1], shape[1])

        if y0 is None or y1 is None or x0 is None or x1 is None:
            continue

        line_arr = np.zeros(labeled.shape, dtype=int)
        rr, cc = sk_line(x1, x0, y1, y0)
        line_arr[rr, cc] = 1

        for label_num in range(1, labeled.max()):
            if label_num == 0:
                continue
            mask = labeled == label_num
            masked = line_arr * mask

            if masked.any():
                intersections.append(label_num)

    return list(set(intersections))


# TODO: refactor
def line_segment_intersect_prob(labeled: np.array, lines: []) -> [int]:
    intersections = []

    for hline in lines:
        p0, p1 = hline
        x0 = p0[0]
        x1 = p0[1]
        y0 = p1[0]
        y1 = p1[1]

        line_arr = np.zeros(labeled.shape, dtype=int)
        rr, cc = sk_line(x1, x0, y1, y0)
        line_arr[rr, cc] = 1

        for label_num in range(1, labeled.max()):
            if label_num == 0:
                continue
            mask = labeled == label_num
            masked = line_arr * mask

            if masked.any():
                intersections.append(label_num)

    return list(set(intersections))


def _draw_lines(hough_lines: [], dimension):
    mask = np.zeros(dimension, dtype=np.uint8)

    for li in hough_lines:
        rr, cc, val = sk_line_aa(li[0][1], li[0][0], li[1][1], li[1][0])
        mask[rr, cc] = val * 255

    return mask
