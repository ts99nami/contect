import time
from datetime import datetime
from multiprocessing import Pool

import pipeline
from analysis.classification import categorise
from common.config import init_params
from common.constants import Key
from common.constants import file_columns, result_columns
from common.fileio import write_csv_dict, write_json, write_csv_pandas


def main(method=None, mask=None):
    # read params
    params = init_params(mask, method)

    # print config
    print('%--------------------------------------%')
    print('Start: ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    print('Number of files: ' + str(len(params[Key.input_files])))
    print('Method: ' + params[Key.method])
    print('Mask: ' + params[Key.mask].upper())
    print('Number of processes: ' + str(params[Key.number_of_processes]))
    print('Output: ' + params[Key.out_dir])
    print('%--------------------------------------%')

    start = time.time()

    # create processes
    pool = Pool(processes=params[Key.number_of_processes])
    jobs = [(input_file, params) for input_file in params[Key.input_files]]

    # run pipeline
    granules = pool.starmap(pipeline.run, jobs)

    end = time.time()
    params['run_time_sec'] = round(end - start, 2)

    # write logs
    write_csv_dict(params[Key.out_proc], granules, file_columns)
    write_json(params[Key.out_conf], params)

    # run analysis and write results
    results = categorise(granules, params)
    write_csv_pandas(results, params[Key.out_res], result_columns)

    print('%--------------------------------------%')
    print('End: ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    print('%--------------------------------------%')


if __name__ == '__main__':
    main()
