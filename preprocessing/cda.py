"""
CONTRAIL DETECTION ALGORITHM
"""

import numpy as np
from scipy.ndimage import gaussian_filter
from skimage.exposure import rescale_intensity
from sklearn.preprocessing import normalize
from copy import copy

from common.constants import Column, Key
from extraction.hdf import get_sds_array, get_sds_path


def _normalize(img: np.array, cda_norm_interval: int) -> np.array:
    return normalize(img) * cda_norm_interval


def _invert(img: np.array) -> np.array:
    return img.max() - img


def _scale(img: np.array, cda_norm_interval: int) -> np.array:
    return (img - np.min(img)) / (np.max(img) - np.min(img)) * cda_norm_interval


def _gaussian_filter(img: np.array, kernel_size: int) -> np.array:
    return gaussian_filter(img, sigma=(kernel_size, kernel_size))


def _btd(band_11: np.array, band_12: np.array) -> np.array:
    return band_12 - band_11


def _calc_n(img: np.array, img_m: np.array, img_std: np.array, k: float) -> np.array:
    return (img - img_m) / (img_std + k)


def _rescale_intensity(img: np.array, out_range) -> np.array:
    return rescale_intensity(img, out_range=out_range)


def preprocess_image(granule: {}, params: {}) -> {}:
    if granule[Column.flight_count] <= 0:
        return granule

    b11_path = get_sds_path(granule[Column.hdf], params[
        Key.band11], params[Key.band_key], params[Key.band_suffix])
    b12_path = get_sds_path(granule[Column.hdf], params[
        Key.band12], params[Key.band_key], params[Key.band_suffix])

    b11 = get_sds_array(b11_path)
    b12 = get_sds_array(b12_path)

    # normalize arrays between 0 and 100
    b11_n = _normalize(b11, params[Key.norm_interval])
    b12_n = _normalize(b12, params[Key.norm_interval])
    b11_ns = _scale(b11_n, params[Key.norm_interval])
    b12_ns = _scale(b12_n, params[Key.norm_interval])

    # calc btd and invert band 12
    td = _btd(b11_ns, b12_ns)
    t5i = _invert(b12_ns)

    # smooth arrays with gaussian 5x5 kernel
    tdm = _gaussian_filter(td, params[Key.kernel_size])
    t5m = _gaussian_filter(t5i, params[Key.kernel_size])

    # calculate std
    sdtd = np.std(td)
    sdt5 = np.std(t5i)

    # combining std, smoothed and inverted
    nd = _calc_n(td, tdm, sdtd, params[Key.cda_k])
    n5 = _calc_n(t5i, t5m, sdt5, params[Key.cda_k])
    n = nd + n5

    granule[Column.processed] = n.copy()
    granule[Column.btd] = td.copy()

    del b11, b12, n, nd, n5, sdtd, sdt5, td, tdm, t5m, t5i

    return granule
